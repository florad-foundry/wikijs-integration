declare class DOMPurify {
  static sanitize(dirty: string): string;
}
