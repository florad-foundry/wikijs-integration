/**
 * This is your TypeScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your module, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your module
 */

// Import TypeScript modules
import { registerSettings } from './module/settings.js';
import WikiHooks from './module/wikiHooks.js';

/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async function () {
  console.log('wikijs-integration | Initializing wikijs-integration');

  // Register custom module settings
  registerSettings();
});

Hooks.on(
  'preUpdateJournalEntry',
  (entity: JournalEntry, data: any, options: any, userId: string) =>
    WikiHooks.onPreUpdateJournalEntry(entity, data, options, userId)
);

Hooks.on(
  'renderJournalDirectory',
  (app: SidebarDirectory, html: JQuery, options: any) =>
    WikiHooks.onRenderJournalDirectory(app, html, options)
);
