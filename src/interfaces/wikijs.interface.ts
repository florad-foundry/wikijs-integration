export interface Page {
  id?: number;
  path?: string;
  hash?: string;
  title?: string;
  description?: string;
  isPrivate?: boolean;
  isPublished?: boolean;
  privateNS?: string;
  publishStartDate?: Date;
  publishEndDate?: Date;
  tags?: PageTag[];
  content?: string;
  render?: string;
  toc: string;
  contentType?: string;
  createdAt?: Date;
  updatedAt?: Date;
  editor?: string;
  locale?: string;
  scriptCss?: string;
  scriptJs?: string;
  authorId?: number;
  authorName?: string;
  authorEmail?: string;
  creatorId?: number;
  creatorName?: string;
  creatorEmail?: string;
}

export interface PageTag {
  id?: number;
  tag?: string;
  title?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface PageListItem {
  id?: number;
  path?: string;
  locale?: string;
  title?: string;
  description?: string;
  contentType?: string;
  isPublished?: boolean;
  isPrivate?: boolean;
  privateNS?: String;
  createdAt?: Date;
  updatedAt?: Date;
  tags?: string[];
}

export interface PageTreeItem {
  id?: number;
  path?: string;
  depth?: number;
  title?: string;
  isPrivate?: boolean;
  isFolder?: boolean;
  privateNS?: string;
  parent?: number;
  pageId?: number;
  locale?: string;
}

export interface PageSearchResponse {
  results: PageSearchResult[];
  suggestions: string[];
  totalHits: number;
}

export interface PageSearchResult {
  id: string;
  title: string;
  description: string;
  path: string;
  locale: string;
}
