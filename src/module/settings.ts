export const registerSettings = function () {
  game.settings.register('wikijs-integration', 'allowUsers', {
    name: game.i18n.localize('WIKI.AllowUsers'),
    hint: game.i18n.localize('WIKI.AllowUsersDesc'),
    scope: 'world',
    config: true,
    type: Boolean,
    default: false
  });
  game.settings.register('wikijs-integration', 'endPoint', {
    name: game.i18n.localize('WIKI.Host'),
    hint: game.i18n.localize('WIKI.HostDesc'),
    scope: 'world',
    config: true,
    type: String,
    default: '',
    onChange: () => location.reload()
  });
  game.settings.register('wikijs-integration', 'apiKey', {
    name: game.i18n.localize('WIKI.ApiKey'),
    hint: game.i18n.localize('WIKI.ApiKeyDesc'),
    scope: 'world',
    config: true,
    type: String,
    default: ''
  });
  game.settings.register('wikijs-integration', 'locale', {
    name: game.i18n.localize('WIKI.Locale'),
    hint: game.i18n.localize('WIKI.LocaleDesc'),
    scope: 'world',
    config: true,
    type: String,
    default: 'en'
  });
};
