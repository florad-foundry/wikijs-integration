import {
  Page,
  PageTreeItem,
  PageSearchResponse
} from '../interfaces/wikijs.interface';
import { IQueryHelperOptions } from '../interfaces/queryHelper.interface';

export default class QueryHelper {
  public static async getRootIndex(
    options: IQueryHelperOptions
  ): Promise<PageTreeItem[]> {
    document.getElementById('loading-indicator').classList.remove('hidden');

    let retVal = [];

    let requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${options.apiKey}`
      },
      body: JSON.stringify({
        query: `{
                pages{
                      tree(mode: ALL, locale: "${options.locale}" includeAncestors:true){
                        id pageId title isFolder parent depth path
                        }
                      }
                }`
      })
    };

    try {
      let response = await fetch(
        options.endPoint.concat('/graphql'),
        requestOptions
      );
      let result = await response.json();
      retVal = result.data.pages.tree;
    } catch (error) {
      console.error(error);
      ui.notifications.error(error);
    } finally {
      document.getElementById('loading-indicator').classList.add('hidden');
    }
    return retVal;
  }

  public static async getPage(
    id: number,
    options: IQueryHelperOptions
  ): Promise<Page> {
    let retVal = null;

    let requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${options.apiKey}`
      },
      body: JSON.stringify({
        query: `{pages{single(id:${id}){id title description render updatedAt}}}`
      })
    };

    try {
      let response = await fetch(
        options.endPoint.concat('/graphql'),
        requestOptions
      );
      let result = await response.json();

      let tempResult = result.data.pages.single as Page;
      tempResult.render = DOMPurify.sanitize(tempResult.render);
      retVal = tempResult;
    } catch (error) {
      console.error(error);
      ui.notifications.error(error);
    } finally {
      document.getElementById('loading-indicator').classList.add('hidden');
    }
    return retVal;
  }

  public static async getFolder(
    id: number,
    options: IQueryHelperOptions
  ): Promise<PageTreeItem[]> {
    let retVal = [];
    document.getElementById('loading-indicator').classList.remove('hidden');

    let requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${options.apiKey}`
      },
      body: JSON.stringify({
        query: `{
                  pages{
                        tree(mode: ALL, locale: "${options.locale}", parent:${id}, includeAncestors:true){
                          id 
                          pageId 
                          title 
                          isFolder 
                          parent 
                          depth 
                          path
                    }
                  }
                }`
      })
    };

    try {
      let response = await fetch(
        options.endPoint.concat('/graphql'),
        requestOptions
      );
      let result = await response.json();
      retVal = result.data.pages.tree;
    } catch (error) {
      console.error(error);
      ui.notifications.error(error);
    } finally {
      document.getElementById('loading-indicator').classList.add('hidden');
    }
    return retVal;
  }

  public static async searchForArticle(
    query: string,
    path: string,
    options: IQueryHelperOptions
  ): Promise<PageSearchResponse> {
    let retVal = null;
    document.getElementById('loading-indicator').classList.remove('hidden');

    let requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${options.apiKey}`
      },
      body: JSON.stringify({
        query: `{
                  pages{
                      search(query:"${query}", path:"${path}" locale:"${options.locale}"){
                          results {id title path}
                          suggestions
                          totalHits
                      }
                  }
                }`
      })
    };

    try {
      let response = await fetch(
        options.endPoint.concat('/graphql'),
        requestOptions
      );
      let result = await response.json();
      retVal = result.data.pages.search;
    } catch (error) {
      console.error(error);
      ui.notifications.error(error);
    } finally {
      document.getElementById('loading-indicator').classList.add('hidden');
    }
    return retVal;
  }
}
