# Wiki.js Integration for Foundry VTT

<div align=center>
<img title="Minimum core version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/florad-foundry/wikijs-integration/-/raw/master/src/module.json&label=core&query=minimumCoreVersion&suffix=%2B&style=flat-square&color=important">
<img title="Latest compatible version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/florad-foundry/wikijs-integration/-/raw/master/src/module.json&label=compatible&query=compatibleCoreVersion&style=flat-square&color=important">
<img src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/florad-foundry/wikijs-integration/-/raw/master/src/module.json&label=version&query=version&style=flat-square&color=success">

[![Donate via Ko-Fi](https://img.shields.io/badge/support-ko--fi-ff4646?style=flat-square&logo=ko-fi)](https://ko-fi.com/florad)
[![Twitter Follow](https://img.shields.io/badge/follow-%40FloRadical-blue.svg?style=flat-square&logo=twitter)](https://twitter.com/FloRadical)

</div>

This module's main goal is to provide an easy and convenient way to browse and import articles from a [Wiki.js](https://wiki.js.org/) instance, allowing it to be used for a kind of things, such as a wolrd-building tool, a repository of tutorials and how-tos, amongst other things.

## Installation Instructions

There are three ways of installing this module

- Use Foundry VTTs built-in Install Module window to find the module in the database. Simply search for the name and hit install
- Paste the following link into the input at the bottom of theInstall Module window: https://gitlab.com/florad-foundry/wikijs-integration/-/raw/master/src/module.json
- build it locally. Instructions for that are further down below

## How to use

Set the wiki Endpoint and API Key in the settings and you're good to go.

## Best Practices

When writing an article in Markdown it's good a good idea to use an `<img>` tag instead normal markdown syntax in case you want to apply styles to the image (such as the `align-right` class) as Wiki.js uses a custom flavour of Markdown
Instead of this

```Markdown
![alt text](https://placekitten.com/408/287){.align-right}
```

use this

```HTML
<img src="https://placekitten.com/408/287" class="align-right">
```

## How to build a version locally

To create a local build of the module follow these steps:

1. If you don't Node.js installed then install the latest Node.js LTS version from here https://nodejs.org/en/
1. Clone the repository and either change into it or open a commandline/terminal window in the cloned the directory
1. Run the `npm install` command to install all the required node modules, including the type definitions.
1. Set the `dataPath` in `foundryconfig.json` to your FoundryVTT data folder.
1. Either run `npm run build` in a shell in your cloned directory or run the npm script `build` directly from your IDE, such as VS Code. Note: you might need to run this command as admin. To do this open your terminal or IDE as admin
1. Done, wikijs-integration should now show up in Foundry VTT as an installed module

## Community Contribution

Code and content contributions are accepted. Please feel free to submit issues to the issue tracker or submit merge requests for code changes. Approval for such requests involves code and (if necessary) design review by FloRad. Please reach out on the Foundry Community Discord with any questions.
